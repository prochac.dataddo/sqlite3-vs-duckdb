package db_bench

import (
	"database/sql"
	"os"
	"path/filepath"
	"testing"

	_ "github.com/marcboeker/go-duckdb"
	"github.com/stretchr/testify/require"
)

func BenchmarkDuckDB(b *testing.B) {
	const query = `SELECT 
    wc.name, cl.column1
FROM world_cities wc
    JOIN country_list cl ON cl.column0 = wc.country
WHERE cl.column1 = 'CZ'
ORDER BY wc.name;
`

	pwd, _ := os.Getwd()

	db, err := sql.Open("duckdb", "")
	require.NoError(b, err)
	b.Cleanup(func() {
		err := db.Close()
		require.NoError(b, err)
	})
	_, err = db.Exec(`CREATE TABLE country_list AS SELECT * FROM read_csv_auto(?)`,
		filepath.Join(pwd, "country-list.csv"),
	)
	require.NoError(b, err)
	_, err = db.Exec(`CREATE TABLE world_cities AS SELECT * FROM read_csv_auto(?)`,
		filepath.Join(pwd, "world-cities.csv"),
	)
	require.NoError(b, err)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rows, err := db.Query(query)
		require.NoError(b, err)
		for rows.Next() {
		}
		err = rows.Err()
		require.NoError(b, err)
		err = rows.Close()
		require.NoError(b, err)
	}
}
