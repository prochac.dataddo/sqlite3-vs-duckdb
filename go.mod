module gitlab.com/prochac.dataddo/sqlite3-vs-duckdb

go 1.20

require (
	github.com/marcboeker/go-duckdb v1.4.0
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/stretchr/testify v1.8.4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
