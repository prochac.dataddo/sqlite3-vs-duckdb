package db_bench

import (
	"database/sql"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
)

func BenchmarkSQLite(b *testing.B) {
	const query = `SELECT 
    wc.name, cl.code
FROM world_cities wc
    JOIN country_list cl ON cl.name = wc.country
WHERE cl.code = 'CZ'
ORDER BY wc.name;
`
	db, err := sql.Open("sqlite3", "sqlite.db?mode=memory")
	require.NoError(b, err)
	b.Cleanup(func() {
		err := db.Close()
		require.NoError(b, err)
	})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rows, err := db.Query(query)
		require.NoError(b, err)
		for rows.Next() {
		}
		err = rows.Err()
		require.NoError(b, err)
		err = rows.Close()
		require.NoError(b, err)
	}
}
