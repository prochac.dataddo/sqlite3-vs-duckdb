# SQLite vs DuckDB

Clear winner is DuckDB! 
It's more than 10x faster.

https://duckdb.org/why_duckdb

```
goos: linux
goarch: amd64
pkg: gitlab.com/prochac.dataddo/sqlite3-vs-duckdb
cpu: Intel(R) Core(TM) i7-7560U CPU @ 2.40GHz
BenchmarkDuckDB
BenchmarkDuckDB-4   	     810	   1452100 ns/op
BenchmarkSQLite
BenchmarkSQLite-4   	      64	  17688417 ns/op
PASS
ok  	gitlab.com/prochac.dataddo/sqlite3-vs-duckdb	3.569s
```
